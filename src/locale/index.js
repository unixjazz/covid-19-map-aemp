import en from "./en.json";
import ptBr from "./pt-BR.json";
import it from "./it.json";
import es from "./es.json";

export default {
  en: {
    translation: en,
  },
  "pt-BR": {
    translation: ptBr,
  },
  es: {
    translation: es,
  },
  it: {
    translation: it,
  },
};
